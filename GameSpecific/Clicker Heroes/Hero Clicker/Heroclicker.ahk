﻿; Heroclicker v0.1.7
 
;; INSTRUCTIONS
; F8 - pause/resume the script
; F9 - toggle slow mode (keeps the clicking combo going but lets you use the mouse to level up heroes etc
; F10 - toggle autoprogress mode (will press "A" every 10 minutes, switching from farm to progress mode - best when busy or AFK)
; F11 - toggle skill mode (will use skills automatically to maximize DPS, performing EDR-ER if Vaagur is maxed)
;
; Edit the next section to configure the script to your liking (REMEMBER TO SET YOUR VAAGUR LEVEL or skills will not work correctly)
 
;; CONFIGURE THE SCRIPT HERE
 
global vaagurLevel := 15 ; set this to the level of your Vaagur ancient (0 if you don't have it, 15 if maxed)
global autoprogressMode := false ; to ALWAYS start the script in manual progress mode, change this to false (toggle modes with F10)
global DPSCooldown := 600 ; in autoprogress mode, switches mode every X seconds (default 600 seconds, 10 minutes)
global skillMode := true ; to ALWAYS start the script in manual skills mode, change this to false (toggle modes with F11)
global pauseScript := false ; to ALWAYS start the script paused, change this to true
global slowMode := false ; to ALWAYS start the script in slow mode, change this to true
 
 
;; END OF CONFIGURATION SECTION - DO NOT EDIT AFTER THIS POINT
 
 
#SingleInstance force
 
global windowTitle := "Clicker Heroes"
SetTitleMatchMode 3
WinActivate %windowTitle%
 
; timing variables
global clickInterval := 40 ; the pause (in ms) between successive clicks
loopPause := 100 ; the pause at the end of the main loop (in ms) to wait for a resume command (F8)
global slowModeInterval := 3000 ; the slow mode interval (in ms) between clicks
 
; timing variables - skills
global cooldownTime5789 := 3600 ; 1h cooldown for skills 5, 7, 8 and 9
 
SetControlDelay -1 ; avoids holding the mouse button down during the click, which reduces interference from user mouse movements
 
lastUsedDPSSkills = %A_Now%
global lastExecutedEDR = %A_Now%
global EDRStarted := false
global ERStarted := false
global EDRCooldown := getCooldown(cooldownTime5789)
global vaagurIsMaxed := checkVaagur()
 
; log
logEvent(" === Heroclicker v0.1.7 was started with Vaagur level " vaagurLevel ", DPS skills cooldown " DPSCooldown " seconds, EDR cooldown " EDRCooldown " seconds ===")
 
; main loop, allows the script to be paused by pressing F8 (which puts control to this loop and checks for another F8 every 100 ms)
Loop {
  while(!pauseScript) {
 
    ; gets the clickables, or (when missing) hits the monster
    getGems()
 
    ; executes skills 123457 every DPSCooldown seconds (default = 600), except between EDR and ER with maxed Vaagur
    DPSSkills()
 
    ; executes EDR-ER at the appropriate time. If Vaagur is not maxed, doesn't block DPS skills between EDR and ER
    EDRER()
 
  } ; end of while(!pauseScript)
 
  Sleep %loopPause%
}
 
 
getGems() {
; try to get the clickables on all 6 positions, or hit monster when missing
 
  ControlClick, X760 Y380, %windowTitle%
  Sleep getSleepTime()
 
  ControlClick, X525 Y485, %windowTitle%
  Sleep getSleepTime()
  ControlClick, X1005 Y450, %windowTitle%
  Sleep getSleepTime()
 
  ControlClick, X870 Y510, %windowTitle%
  Sleep getSleepTime()
 
  ControlClick, X1050 Y440, %windowTitle%
  Sleep getSleepTime()
 
  ControlClick, X750 Y430, %windowTitle%
  Sleep getSleepTime()
 
  return
}
 
getSleepTime() {
 
  if(slowMode)
    return slowModeInterval
  else
    return clickInterval
}
 
; returns the actual cooldown time, taking the level of Vaagur into consideration
getCooldown(cooldownTime) {
 
  return floor(cooldownTime*(1-vaagurLevel*5/100))
}
 
checkVaagur() {
 
  return (vaagurLevel = 15)
}
 
; log string to file with date and time, for debug purposes
logEvent(s) {
 
  FileAppend, %A_Hour%:%A_Min%:%A_Sec%: %s% `n, debug.txt
  return
}
 
; executes skills 123457 every DPSCooldown seconds, except between EDR and ER with maxed Vaagur
DPSSkills() {
  global ; assumes all variables used in this function are global
 
  ; calculate elapsed time since DPS skills were last used
  timeSinceDPS = %A_Now%
 
  EnvSub, timeSinceDPS, lastUsedDPSSkills, seconds
 
  ; use DPS skills, unless you're between EDR and ER with maxed out Vaagur
  if (skillMode && timeSinceDPS > DPSCooldown && !(EDRStarted && !ERStarted && vaagurIsMaxed)) {
    ; switch progress mode before using these skills, so that they are used on a difficult boss
    if(autoprogressMode)
      ControlSend,, a, %windowTitle%
 
    ; use skills
    ControlSend,, 1, %windowTitle%
    ControlSend,, 2, %windowTitle%
    ControlSend,, 3, %windowTitle%
    ControlSend,, 4, %windowTitle%
    ControlSend,, 5, %windowTitle%
    ControlSend,, 7, %windowTitle%
     
    ; reset timer to now
    lastUsedDPSSkills = %A_Now%
    logEvent("DPS skills used")
  }
 
  return
}
 
EDRER() {
  global ; assumes all variables used in this function are global
 
  ; EDR-ER, but if Vaagur is not maxed don't block DPS skills (still using ER)
  if (skillMode && !EDRStarted) {
    EDRStarted := true
     
    ; EDR
    ControlSend,, 8, %windowTitle%
    ControlSend,, 6, %windowTitle%
    ControlSend,, 9, %windowTitle%
 
    ; reset timer to now
    lastExecutedEDR = %A_Now%
     
    ; log EDR
    if(vaagurLevel = 15)
      logEvent("EDR executed: Vaagur is maxed, so DPS skills are blocked until ER")
    else
      logEvent("EDR executed")
  }
 
  ; wait 15 min-1h (EDRCooldown seconds as calculated before)
  timeSinceEDR = %A_Now%
  EnvSub, timeSinceEDR, lastExecutedEDR, seconds
 
  if (skillMode && !ERStarted && timeSinceEDR > EDRCooldown) { ; time to perform ER
    ERStarted := true
 
    ; ER
    ControlSend,, 8, %windowTitle%
    ControlSend,, 9, %windowTitle%
 
    ; reset timer to now
    lastExecutedEDR = %A_Now%
    logEvent("ER executed")
 
  }
 
  ; wait 15m-1h and then set EDRStarted and ERStarted back to false
  timeSinceEDR = %A_Now%
  EnvSub, timeSinceEDR, lastExecutedEDR, seconds
 
  if (ERStarted && timeSinceEDR > EDRCooldown) {
    EDRStarted := false
    ERStarted := false
  }
 
  return
}
 
 
; === SHORTCUTS ===
 
; Pausing shortcut
F8::
    pauseScript := !pauseScript
 
    ; confirmation message (to make sure you don't lose combo by accident)
    if(pauseScript) {
      logEvent("Script was paused")
      MsgBox,, PAUSED, Autoclicker was paused, 1
    } else {
      logEvent("Script was resumed")
      MsgBox,, RESUMED, Autoclicker was resumed, 1
    }
    return
 
; Slow mode shortcut
F9::
  slowMode := !slowMode
 
  ; confirmation message
  if(slowMode) {
    logEvent("Slow mode enabled")
    MsgBox,, SLOW MODE, Autoclicker is in slow mode, 1
  } else {
    logEvent("Normal speed resumed")
    MsgBox,, NORMAL MODE, Autoclicker is at full speed, 1
  }
  return
 
F10::
  autoprogressMode := !autoprogressMode
 
  ; confirmation message
  if(autoprogressMode) {
    logEvent("Autoprogress mode enabled")
    MsgBox,, AUTOPROGRESS, Switching from farm to progress mode every %DPSCooldown% seconds`n(best when AFK or farming), 3
  } else {
    logEvent("Manual progress mode enabled")
    MsgBox,, MANUAL PROGRESS, Manual progress mode activated `n(best when actively playing or when you are one-shotting monsters), 3
  }
  return
 
F11::
  skillMode := !skillMode
 
  ; confirmation message
  if(skillMode) {
    logEvent("Autoskills enabled")
    MsgBox,, AUTOMATIC SKILLS, Will automatically call skills to maximize DPS, 2
  } else {
    logEvent("Manual skills enabled")
    MsgBox,, MANUAL SKILLS, Autoskills disabled, 2
  }
  return
 
return