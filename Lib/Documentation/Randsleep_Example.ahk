﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


; Use format "hr:min:sec:ms"
; This would randomize sleep between between 11 minutes 30 seconds
; and 1 hour 30 minutes 25 seconds
F1::SleepRand("0:11:30:0", "1:30:25:0")

; Sleep timer for between 15 and 20 seconds
F2::SleepRand("0:0:15:0", "0:0:20:0")
