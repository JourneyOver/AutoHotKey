﻿;MGlossary 1.0 — a simple glossary script
; — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — 
; INSTRUCTIONS:
;
; 1) You can include terms in the glossary, highlighting a term and using F12.
; This will feed the glossary file (glossario.ini).
; 2) If you are translating something and you find a term that you know is in the glossary, you can select it
; and click F11.
; This will replace the term for its match in the file glossario.ini (the file we use to build the glossary).
; — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —
;
;------------------------------------------------------------------------------
; Settings
;------------------------------------------------------------------------------
#NoEnv ; For security
#SingleInstance force
;Menu, Tray, Tip, Glossary
#NoTrayIcon

;------------------------------------------------------------------------------
; Close script on Home.exe close
;------------------------------------------------------------------------------

Loop {
  Process,Exist,Home.exe ;name of the executable
  If !ErrorLevel
  ExitApp
  Sleep,3000
}

; — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — 
; Use F12 to enter the glossary terms. They will be added to the glossary file.
; — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — 

F12::
  ; Get the selected text.
  AutoTrim Off ; Retain any leading and trailing whitespace on the clipboard.
  ClipboardOld = %ClipboardAll%
  Clipboard = ; Must start off blank for detection to work.
  Send ^c
  ClipWait 1
  if ErrorLevel ; ClipWait timed out.
return
; Replace CRLF and/or LF with `n for use in a “send-raw” hotstring:
; The same is done for any other characters that might otherwise
; be a problem in raw mode:
StringReplace, Hotstring, Clipboard, ``, ````, All ; Do this replacement first to avoid interfering with the others below.
StringReplace, Hotstring, Hotstring, `r`n, ``r, All ; Using `r works better than `n in MS Word, etc.
StringReplace, Hotstring, Hotstring, `n, ``r, All
StringReplace, Hotstring, Hotstring, %A_Tab%, ``t, All
StringReplace, Hotstring, Hotstring, `;, ```;, All
Clipboard = %ClipboardOld% ; Restore previous contents of clipboard.
; This will move the InputBox’s caret to a more friendly position:
SetTimer, MoveCaret, 10
; Show the InputBox, providing the default hotstring:
InputBox, Hotstring, New glossary term, Provide the target on the right side. You can also edit the left side,,,,,,,, %Hotstring%=%Hotstring%

if ErrorLevel <> 0 ; The user pressed Cancel.
return
; Otherwise, add the hotstring and reload the script:
FileAppend, `n%Hotstring%, glossario.ini ; Put a `n at the beginning in case file lacks a blank line at its end.
Reload
Sleep 200 ; If successful, the reload will close this instance during the Sleep, so the line below will never be reached.
MsgBox, 4,, The hotstring just added appears to be improperly formatted. Would you like to open the script for editing? Note that the bad hotstring is at the bottom of the script.
IfMsgBox, Yes, Edit
  return

MoveCaret:
  IfWinNotActive, New Hotstring
    return
  ; Otherwise, move the InputBox’s insertion point to where the user will type the abbreviation.
  Send {HOME}
  Loop % StrLen(Hotstring) + 4
  SendInput {Right}
  SetTimer, MoveCaret, Off
return

#Hotstring R ; Set the default to be “raw mode” (might not actually be relied upon by anything yet).

F11::
OldClip := ClipboardAll
Clipboard = ; Clear clipboard for ClipWait.
Send,^c ; Copy selection.
ClipWait, 1 ; ClipWait for reliability.
Word := Clipboard
if Word is space
{
  Word:= “error” ; This is to avoid the catastrophic error of sending the whole file!
}
IniRead, Repl, glossario.ini, words, %Word%
; Read word replacement from ini file.
if Repl != ERROR ; If this word has a replacement, send it.
{
  ; Use same casing as what was typed (for the first character.)
  c := SubStr(Word,1,1)
  if (c = SubStr(Repl,1,1))
  Repl := c . SubStr(Repl,2)

  ;KeyWait, LWin ; Wait for LWin to be released, or Windows Vista may do stupid things.
  Send %Repl%
  Clipboard := OldClip
} else {
  Clipboard := OldClip
  ; To have a ToolTip disappear after a certain amount of time
  ; without having to use Sleep (which stops the current thread):
  #Persistent
  ToolTip, Sorry I haven’t learned this yet
  SetTimer, RemoveToolTip, 3000
  return

  RemoveToolTip:
  SetTimer, RemoveToolTip, Off
  ToolTip
  return
}
