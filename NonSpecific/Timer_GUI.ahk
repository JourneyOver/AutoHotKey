﻿StartTime := A_TickCount
Gui -ToolWindow +AlwaysOnTop
Gui, Font, s32, Tahoma
Gui, Add, Text, w320 h48 c00FF32 vTimer +Center, 00:00:00:00.000
Gui, Color, 000000
Gui, Show

loop
{
NowTime := A_TickCount - StartTime
ms := Mod(NowTime, 1000)
s := Mod((NowTime-ms)/1000,60)
m := Mod((((NowTime-ms)/1000)-s)/60,60)
h := Mod(((((((NowTime-ms)/1000)-s)/60)-m)/60),24)
d := ((((((((NowTime-ms)/1000)-s)/60)-m)/60)-h)/24)
SetFormat , Float, 02.0
if ms > 99
{
        GuiControl , Text , Timer , %d%:%h%:%m%:%s%.%ms%
}
else
if ms > 9
{
        GuiControl , Text , Timer , %d%:%h%:%m%:%s%.0%ms%
}
else
{
        GuiControl , Text , Timer , %d%:%h%:%m%:%s%.00%ms%
}
}
