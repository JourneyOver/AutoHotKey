; User Profile
global profileDir := "C:\Users\JourneyOver\"

; Chrome class + path
global chromeClass := "Chrome_WidgetWin_1"
global chromePath := A_AppData . "\Local\CentBrowser\Application\chrome.exe"

; DropBox window name + path
;global dropWName := "Dropbox"
;global dropPath := profileDir . "Dropbox"

; Download name + path
global downloadWName := "Downloads"
global downloadPath := profileDir . "Downloads"

; My Documents window name
global mydocWName := "My Documents"

; My Pictures windows name + path
global mypicturesWName := "My Pictures"
global mypicturePath := profileDir . "Pictures"

; My Movies/TV Shows windows name + path
global myvideosWName := "My Movies/TV Shows"
global myvideosPath := "D:\Shows"
